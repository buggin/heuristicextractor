# open file
# cutoff de blacklist e percentuale
# pusha a rabbit i link

amqp = require 'amqplib/callback_api'
reader = require 'line-reader'
_ = require 'lodash'
urllib = require 'url'

file = 'dump-consumer.log'

black = [
  'longislandexchange.com',
  'newhub.shafaqna.com',
  'bakeca.it/',
  'bakecaincontrii.com',
  'seguiprezzi.it',
  'dir-companies.com',
]
array = []

amqp.connect 'amqp://ebezzi:ebezzi@ema', (err, conn) -> 
  console.log err if err
  conn.createChannel (err, ch) ->

    push = (link) ->
      ch.assertQueue "extractor"
      ch.sendToQueue "extractor", new Buffer(link)

    reader.eachLine file, (line, last) ->
      if /INFO/.test line 
        if not /comments/.test line
          m = /http[s]?:\/\/[-a-zA-Z0-9+&@#\/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#\/%=~_|]/.exec line
          if m
            link = m[0]
            array.push link
            # console.log "Pushed ", link
    .then () ->
      console.log "prev: #{array.length}\ncomputing uniq"
      array = _.uniq array, (x) -> urllib.parse(x).hostname
      console.log "uniq: #{array.length}\npopping from blacklist"
    
      for link in array
        for b in black
          if RegExp(b).test link
            array.pop link

      for link in array  
        push link
        console.log "Rabbited ", link
