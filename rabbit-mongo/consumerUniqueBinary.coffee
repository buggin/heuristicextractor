
# fai le richieste per ogni link 
# salva in binario url e data

request = require 'request'
moment = require 'moment'
_ = require 'lodash'

MongoClient = require('mongodb').MongoClient

log4js = require 'log4js'
log = log4js.getLogger()

amqp = require('amqplib').connect 'amqp://ebezzi:ebezzi@ema'

load = (url, callback) ->
  options = 
    url: url
    timeout: 10000
    encoding: null
    headers: 
      'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36'

  request options, (err, resp, data) ->
    callback err, data

MongoClient.connect "mongodb://ema:27017/extractor", (err, db) ->
  links = db.collection "binary"

  amqp.then (conn) ->

    conn.createChannel().then (linkChannel) ->
      linkChannel.assertQueue 'extractor'
      linkChannel.prefetch 25

      linkChannel.consume 'extractor', (msg) ->
        link = msg.content.toString()

        load link, (err, data) ->
          linkChannel.ack msg
          return log.error err.message if err
          obj = {url: link, data: data}

          links.insert obj, (err) ->
            return log.error err.message if err
            log.info "inserted " + obj.url