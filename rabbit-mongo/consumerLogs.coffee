moment = require 'moment'
_ = require 'lodash'

MongoClient = require('mongodb').MongoClient

log4js = require 'log4js'
#log4js.configure 'logging.json'
log = log4js.getLogger()

extractor = require('./heuristicExtractor')
amqp = require('amqplib').connect 'amqp://ebezzi:ebezzi@ema'

MongoClient.connect "mongodb://ema:27017/extractor", (err, db) ->
  links = db.collection "data"

  amqp.then (conn) ->

    conn.createChannel().then (linkChannel) ->
      linkChannel.assertQueue 'extractor'
      linkChannel.prefetch 25

      linkChannel.consume 'extractor', (msg) ->
        link = msg.content.toString()

        extractor.fetchData link, (err, obj) ->
          linkChannel.ack msg
          return log.error err.message if err    

          links.insert obj, (err) ->
            return log.error err.message if err
            log.info "inserted " + obj.url