amqp = require 'amqplib/callback_api'
reader = require 'line-reader'

file = 'spider-link-consumer.log'
amqp.connect 'amqp://ebezzi:ebezzi@ema', (err, conn) -> 
  console.log err if err
  conn.createChannel (err, ch) ->

    push = (link) ->
      ch.assertQueue "extractor"
      ch.sendToQueue "extractor", new Buffer(link)

    reader.eachLine file, (line, last) ->
      if /INFO/.test line 
        if not /comments/.test line
          m = /https?:\/\/.*/.exec line
          if m
            link = m[0]
            push link
            console.log "Pushed", link

