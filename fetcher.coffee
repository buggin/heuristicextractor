request = require 'request' 
cheerio = require 'cheerio'


getHTML = (url, callback) ->
  options = 
      url: url
      timeout: 10000
      headers: 
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36'
  request options, (err, response, body) -> 
    return callback err if err
    return callback new Error("Invalid status code: " + response.statusCode) if response.statusCode is not 200 
    callback undefined, body

getDOM = (url, callback) ->
  getHTML url, (err, html) ->
    return callback err if err
    callback null, cheerio.load(html)

exports.getDOM = getDOM
exports.getHTML = getHTML