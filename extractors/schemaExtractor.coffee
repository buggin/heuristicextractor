request = require 'request'
cheerio = require 'cheerio'
_ = require 'lodash'
sanitizeHtml = require 'sanitize-html'
iconv = require 'iconv-lite' 
_s = require 'underscore.string'
urllib = require 'url'
schema = require '../schema/schema'

class Document
  constructor: (@url, html) ->
    @doc = cheerio.load html
    @removeCommentsHtml(html)
    @removeCommentsDiv()

  wrap: (e) -> #idempotente
    @doc(e)

  select: (selector) ->
    @doc(selector)

  cleanRelated: (div) ->
    black = ['related', 'correlati', 'crp_related', 'wp_rp_content', 'widget', 'comment',
     'altro', 'altri', 'share', 'box', 'menu', 'social', 'share', 'sharing',
      'articleIconLinksContainer', 'most-read', 'breadcrumbs', 'toolbar', 'bottom',
       'extras', 'tabbed', 'newer', 'tool', 'facts', 'releated', 'veeseo', 'photo',
        'image', 'footer', 'overlay', 'UltimiArticoli', 'addthis',]
    for word in black
      # @select("[class*=#{word}]:not(body):not(html), [id*=#{word}]:not(body):not(html)").remove()
      @doc("[class*=#{word}]:not(body):not(html), [id*=#{word}]:not(body):not(html)", @wrap(div)).remove()
      # @doc("[class*=#{word}], [id*=#{word}]", @wrap(div)).remove()



  isProbablyHomePage : () ->
    uri = urllib.parse @url
    return false if uri.query
    return true if not uri.pathname
    tokens = _.compact uri.pathname?.split '/'
    return false if tokens.length > 2
    return false if _.any (x in uri.pathname for x in ['_', '%', ' '])
    return false if _s.count(uri.pathname, '-') > 1
    return false if /[0-9]{4,}/.test uri.pathname
    true

  lentext : (e) ->
    e = @wrap(e)
    tags = ["b", "i", "font", "strong", "span", "p", "h4",
     "h3", "h2", "h1", "em", "blockquote", "cite"]
    tx = _.reduce (c.data?.trim().length for c in e.contents() when c?.type is 'text'), (x,y) -> x+y  
    tx = tx or 0
    for c in e.contents()
      if c.type is 'tag' and c.name in tags
        tx += @lentext c
    tx or 0

  removeCommentsHtml : (e) ->
    e = @wrap(e)
    for c in e.contents()
      if c.type is 'comment'
        @wrap(c).remove()
      if c.type is 'tag'
        @removeCommentsHtml c

  removeCommentsDiv : () ->
    black = ['footer', 'comments', 'commenti']
    for word in black
      @select("[class*=#{word}]:not(body):not(html), [id*=#{word}]:not(body):not(html)").remove()

    @select('footer, aside').remove()
    @select('script, style').remove()

  checkSiblings : (e) ->
    e = @wrap(e)
    s = @wrap(sibling)
    classes = e.attr('class')
    for sibling in e.prevAll()
      if s.attr('class') is classes
        e.prepend(sibling)
    for sibling in e.nextAll()
      if s.attr('class') is classes
        e.append(sibling)
    return e

  getSchemaDiv : () ->
    types = [
      {type: "NewsArticle", clazz: "articleBody"},
      {type: "Article", clazz: "articleBody"},
      {type: "BlogPosting", clazz: "articleBody"},
      {type: "WebPage", clazz: "text"},
      {type: "Review", clazz: "reviewBody"},
    ]

    for {type, clazz} in types
      div = @select("[itemprop~='#{clazz}']")

      content = div.attr("content") #se e' nei meta-tag
      return cheerio("<div>#{content}</div>") if content and content.length
      if div.length > 1
        return cheerio ("<p>#{d}</p>" for d in div)
      return div
      
  getText : () ->
    schema = @getSchemaDiv()
    if schema.html()
      div = schema
    else
      div = @getMaxDiv()
      # div = @checkSiblings div

    @cleanRelated(div)
    return @sanitize @wrap(div).html()

  sanitize : (html) ->
    return sanitizeHtml(html) if html

  parse : (options) ->
    dataUtf8 = data.toString()
    $ = cheerio.load dataUtf8, options
    try
      enc1 = $('meta[http-equiv="Content-Type"]').attr('content').match(/charset=([-\w]*)/)[1]
    catch e
      null
    enc2 = $('meta[charset]').attr('charset')
    enc = enc1 or enc2
    
    if enc in ['latin1', 'iso-8859-1', 'ISO-8859-1']
      enc = 'windows-1252'

    if not enc or enc in ['utf-8', 'UTF-8', 'utf8']
      return $

    dataEnc = iconv.decode data, enc
    return cheerio.load dataEnc, options

  getNormalizedUrl : () ->
    try
      normalizedUrl = @select('link[rel="canonical"]').attr('href') \
        or @select('meta[property="og:url"]').attr('content')
      normalizedUrl = urllib.resolve(url, normalizedUrl) if normalizedUrl

      if normalizedUrl and not isProbablyHomePage(normalizedUrl)
        normalizedUrl
    catch e
      undefined

  getMaxDiv : () ->
    divs = (div for div in @select('div, td, article, section'))
    maxdiv = _.max(divs, (x) => @lentext x)
    @wrap(maxdiv)

  picture : () ->
    @select('meta[property="og:image"]').attr('content') \
      or @select('meta[name="twitter:image"]').attr('content') \
      or @select('meta[name="og:thumb"]').attr('content') \
      or @select('link[rel="image_src"]').attr('href')

  title : () ->
    @select('meta[property="og:title"]').attr('content') \
      or @select('meta[name="twitter:title"]').attr('content') \
      or @select('meta[name="title"]').attr('content') \
      or @select('title').text()

  description : () ->
    @select('meta[property="og:description"]').attr('content') \
      or @select('meta[name="twitter:description"]').attr('content') \
      or @select('meta[name="description"]').attr('content')

  keywords : () ->
    @select('meta[name="keywords"]').attr('content') \
      or @select('meta[name="tags"]').attr('content')

fetchFromSpecificInfo = (doc, url) ->

fetchDataFromHTML = (html, url) ->

  doc = new Document url, html

  try
    doc.parse
  catch e
    console.log e

  obj = 
    text: doc.getText()
    # title: doc.title()
    # description: doc.description()
    # picture: doc.picture()
    # keywords: doc.keywords()
    url: url
    # type: 'web'

  return obj

fetchData = (url, callback) ->
  options = 
    url: url
    timeout: 10000
    encoding: null
    gzip: true
    headers: 
      'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36'

  request options, (err, resp, data) ->
    throw err if err 
    throw new Error("Status code: #{resp.statusCode}") if resp.statusCode isnt 200
    obj = fetchDataFromHTML data, resp.request.uri.href
    callback err, obj 


exports.fetchData = fetchData
exports.fetchDataFromHTML = fetchDataFromHTML
