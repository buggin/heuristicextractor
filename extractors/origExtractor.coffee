request = require 'request'
cheerio = require 'cheerio'
_ = require 'lodash'
sanitizeHtml = require 'sanitize-html'
iconv = require 'iconv-lite' 
_s = require 'underscore.string'
urllib = require 'url'


isProbablyHomePage = (url) ->
  uri = urllib.parse url
  return false if uri.query
  return true if not uri.pathname

  tokens = _.compact uri.pathname?.split '/'

  return false if tokens.length > 2
  return false if _.any (x in uri.pathname for x in ['_', '%', ' '])
  return false if _s.count(uri.pathname, '-') > 1
  return false if /[0-9]{4,}/.test uri.pathname
  return true

lentext = (e) ->
  e = cheerio(e)
  tags = ["b", "i", "font", "strong", "span", "p", "h4",
   "h3", "h2", "h1", "em", "blockquote", "cite"]
  tx = _.reduce (c.data?.trim().length for c in e.contents() when c?.type is 'text'), (x,y) -> x+y  
  tx = tx or 0
  for c in e.contents()
    if c.type is 'tag' and c.name in tags
      tx += lentext c
  return tx or 0

removeComments = (e) ->
  e = cheerio(e)
  for c in e.contents()
    if c.type is 'comment'
      cheerio(c).remove()
    if c.type is 'tag'
      removeComments c

cleanRelated = (doc, e) ->
  # + ['recent-posts', 'ultimiArticoliNews', 'nav', 'Privacy']
  # - ['box']
  black = ['related', 'correlati', 'crp_related', 'wp_rp_content', 'widget', 'comment',
   'altro', 'altri', 'share', 'box', 'menu', 'social', 'share', 'sharing',
    'articleIconLinksContainer', 'most-read', 'breadcrumbs', 'toolbar', 'bottom',
     'extras', 'tabbed', 'newer', 'tool', 'facts', 'releated', 'veeseo', 'photo',
      'image', 'footer', 'overlay', 'UltimiArticoli']
  for word in black
    doc("[class*=#{word}]:not(body):not(html), [id*=#{word}]:not(body):not(html)", doc(e)).remove()

sanitize = (html) ->
  return sanitizeHtml(html) if html

parse = (data, options) ->
  dataUtf8 = data.toString()
  $ = cheerio.load dataUtf8, options
  try
    enc1 = $('meta[http-equiv="Content-Type"]').attr('content').match(/charset=([-\w]*)/)[1]
  catch e
    null
  enc2 = $('meta[charset]').attr('charset')
  enc = enc1 or enc2
  
  if enc in ['latin1', 'iso-8859-1', 'ISO-8859-1']
    enc = 'windows-1252'

  if not enc or enc in ['utf-8', 'UTF-8', 'utf8']
    return $

  dataEnc = iconv.decode data, enc
  return cheerio.load dataEnc, options

fetchFromSpecificInfo = (doc, url) ->
  info = 
    'corriere.it'             : 'div.container-body-article'
    'blogspot'                : 'div.post-body.entry-content'
    'free-italia.net'         : 'div.post-body.entry-content'
    'virtualrome.com'         : 'div[class="entry clearfix"] p'
    'assinews.it'             : 'span.detail_article_description'
    'informarexresistere.fr'  : 'div.shortcode-content'
    'primonumero.it'          : 'div.text, p[align="justify"]'
    'gazzetta.it'             : 'div.body-article'
    'bluerating.com'          : 'div#fullarticle table td[style="padding-top:20px;"] div[style="color:#042241;font-size:12px;"]'
    'travelnews24.it'         : 'div.pages__body'
    'modenanoi.it'            : 'div.testo'
    'vibrotekvolley.it'       : 'span.testo'
    'spaziointer.it'          : 'div.postcontent'

  for site, path of info
    if RegExp(site).test urllib.parse(url).hostname
      res = doc(path)
      return res if res.length

fetchDataFromHTML = (html, url) ->
  try
    $ = parse html
  catch e
    throw e
  black = ['footer', 'comments', 'commenti']
  for word in black
    $("[class*=#{word}]:not(body):not(html), [id*=#{word}]:not(body):not(html)").remove()

  $('footer, aside').remove()
  $('script, style').remove()
  removeComments $.root()

  divs = (div for div in $('div, td, article, section'))
  maxdiv = fetchFromSpecificInfo($, url) or _.max(divs, (x) -> lentext x)
  # maxdiv = _.max(divs, (x) -> lentext x)
  cleanRelated $, $(maxdiv)

  obj = 
    text: sanitize $(maxdiv).html()
    title: $('meta[property="og:title"]').attr('content') \
      or $('meta[name="twitter:title"]').attr('content') \
      or $('meta[name="title"]').attr('content') \
      or $('title').text()
    description: $('meta[property="og:description"]').attr('content') \
      or $('meta[name="twitter:description"]').attr('content') \
      or $('meta[name="description"]').attr('content')
    picture: $('meta[property="og:image"]').attr('content') \
      or $('meta[name="twitter:image"]').attr('content') \
      or $('meta[name="og:thumb"]').attr('content') \
      or $('link[rel="image_src"]').attr('href')
    keywords: $('meta[name="keywords"]').attr('content') \
      or $('meta[name="tags"]').attr('content')
    url: url
    type: 'web'
    maxdiv: maxdiv

  # Normalized url must be pushed iff exists and it is not a homepage
  # To be safe, wrap the whole thing in a try/catch
  try
    normalizedUrl = $('link[rel="canonical"]').attr('href') \
      or $('meta[property="og:url"]').attr('content')
    normalizedUrl = urllib.resolve(url, normalizedUrl) if normalizedUrl

    if normalizedUrl and not isProbablyHomePage(normalizedUrl)
      obj.normalizedUrl = normalizedUrl
  catch e
    console.error e
    null

  return obj

fetchData = (url, callback) ->
  options = 
    url: url
    timeout: 10000
    encoding: null
    headers: 
      'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36'

  request options, (err, resp, data) ->
    try
      throw err if err
      obj = fetchDataFromHTML data, resp.request.uri.href
      callback err, obj 
    catch e
      callback e, {}

exports.fetchData = fetchData
exports.fetchDataFromHTML = fetchDataFromHTML
