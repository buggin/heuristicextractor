$ = require 'cheerio'
format = (node) ->
	node = $(node)
	node[0]?.name + (if node.attr("class") then "." +  node.attr("class") else "") + (if node.attr("id") then "#"+ node.attr('id') else "") #+ " [length: " + computeLengthFLText(node) + "]"

getParents = (node) ->
	node = $(node)
	parents = []
	fnode = format node
	parents.push fnode #if fnode.attr("class") or fnode.attr("id")
	node.parentsUntil( $.root()).each (i, elem) ->
	  parents.push ( format elem)
	parents = parents.reverse().join(' -> ')
	return parents

exports.format = format
exports.printParents = getParents