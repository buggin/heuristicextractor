$= require 'cheerio'
wrapRequest = require './wrapRequest'
extractor = require './extractor'

url = process.argv[2]

# selectors = [
# 	'div.entry-content',
# 	'div.body-text']
# 
# debug = (url,selectors) ->
# 	wrapRequest.wrapRequest url, (err, body) ->
# 		return console.log err if err
# 		console.log extractor.getTextPs(body)
# 		console.log extractor.countTextPs(body(selectors[0]))
# 		for elem in body(selectors[0]).contents()
# 			if elem.type is 'text'
# 				console.log "#{elem.data.trim()}"
# 
# debug url, selectors

debug = (url) ->
	extractor.extract url, (err, data) ->
		console.log err if err
		console.log data

debug url