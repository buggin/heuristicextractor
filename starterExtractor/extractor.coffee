wrapRequest = require './wrapRequest'
$ = require 'cheerio'
format = require './format'
_ = require 'lodash'

maxDiv = (divs) ->
    _.max(divs, countTextPs)

getTextPs = (node) ->
    text = []
    # text = ($(child).text() for child in $(node).children() when /^(p|span|h1|h2|h3|h4|b|i|font|em|blockquote|cite|strong)$/i.test child.tagName)
    text = ($(child).text() for child in $(node).children() when /^(p|span)$/i.test child.tagName)
    
    for elem in $(node).contents()
        if elem.type is 'text'
            text.push elem.data.trim()
    return text.join()

countTextPs = (node) ->
    getTextPs(node).length

extract = (url, callback) ->
    wrapRequest.wrapRequest url, (err, body) -> 
        return callback err if err
        
        black = ['footer', 'comments', 'commenti']
        for word in black
            body("[class*=#{word}], [id*=#{word}]").remove()

        callback undefined, format.printParents(maxDiv(body('div,section,article,td')))

exports.extract = extract
exports.getTextPs = getTextPs
exports.countTextPs = countTextPs