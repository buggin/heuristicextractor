cheerio = require 'cheerio'
wrapRequest = require './wrapRequest.js'
format = require './format.js'
#url = "http://www.ansa.it/sito/notizie/cronaca/2015/03/24/un-airbus-della-german-wing-si-e-schiantato-nel-sud-della-francia_039dca3e-6b89-4b2d-a4de-ee11af0c7f8c.html"
url = process.argv[2]

DOMPrint = (obj, level=0) ->
	obj = cheerio(obj)
	console.log (Array(level).join(' ') + format.format(obj))
	for child in obj.children()
		DOMPrint child, ++level
		level--

wrapRequest.wrapRequest url, DOMPrint


# extractFirstLevelTextFrom = (elm) -> 
# 	text = ''
# 	for i in elm.childNodes.length
#  		if elm.childNodes[i].nodeType==3
#  			text += elm.childNodes[i].nodeValue
#  	return text
