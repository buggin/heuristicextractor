origExtractor = require ('/opt/work/node-intl/extractor')
newExtractor  = require ('/opt/work/newExtractor/extractor')
$ = require('cheerio')
format = require ('./format')
urls = [ 
	#'http://napoli.repubblica.it/cronaca/2015/03/30/news/ischia_arrestato_il_sindaco_con_l_accusa_di_tangenti-110804998/?ref=HRER3-1',
	#'http://www.ilfattoquotidiano.it/2015/03/27/andreas-lubitz-pilota-cercato-buttare-porta-cabina-unascia/1541677/',
	#'http://espresso.repubblica.it/palazzo/2015/03/30/news/il-pd-si-spacca-sull-italicum-matteo-renzi-sul-il-passaggio-alla-camera-pensiamo-alla-fiducia-1.206460?ref=HEF_RULLO'
	'http://www.airpressonline.it/5479/simulando-simpara-lenav-academy-raccontata-un-libro/'
]

for url in urls

	origExtractor.fetchData url, (err, obj) ->
		console.log "---orig: #{format.printParents obj.maxdiv}"

	newExtractor.extract url, (err, obj) ->
		console.log "====new: #{obj}" 
