###Workflow indicativo

usando l'estrattore in produzione, prendere gli url che non vanno e dividerli in classi di problemi

* testo vuoto
* prende i correlati
* prende solo il maggiore tra i paragrafi

dato un subset di casi con quei problemi,

si ragiona su quali parametri possono essere utili per evidenziare se un caso possa appartenere ad una classe di problemi

si estraggono dal flusso grezzo i dati con l'aggiunta dei parametri che ci possono indicare a quale classe di problemi possano appartenere

si testa se la soluzione e' migliore