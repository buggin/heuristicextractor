MongoClient = require('mongodb').MongoClient
async = require 'async'
schema = require '../extractors/schemaExtractor'
orig = require '../extractors/origExtractor' 
request = require 'request'
fs = require 'fs'
_ = require 'lodash'
urllib = require 'url'

jaccard = (first, second) ->
  return 0 if not first or not second
  tokensFirst = first.split /\W+/
  tokensSecond = second.split /\W+/
  union = _.union(tokensFirst, tokensSecond)
  intersection = _.intersection(tokensFirst, tokensSecond)
  return _.uniq(intersection).length / _.uniq(union).length

fs.open "html/schema.html", "w", (err, fd) ->
  MongoClient.connect 'mongodb://ema:27017/extractor', (err, db) ->
    db.collection('binary').find().each (err, doc) ->
      if not doc
        console.log "Done!"
        return
      try
        v1 = schema.fetchDataFromHTML doc.data.toString(), doc.url
        v2 = orig.fetchDataFromHTML doc.data.toString(), doc.url
        if not v1.text and not v2.text
          jci = 1
        else if v1.text and not v2.text or not v1.text and v2.text
          jci = 0
        else
          jci = jaccard v1.text, v2.text
        if jci < 0.8
          table = """
              <table border=1>
                <tr><td colspan=2><a href="#{doc.url}">#{jci} : #{doc.url}</a></td></tr>
                <tr><td>#{v1?.text}</td><td>#{v2?.text}</td></tr>
              </table>
              <br><br>
            """
          console.log "Diff found: #{jci}"
          console.log "~~~~schema~~~~#{v1?.text} \n@@@@orig@@@@#{v2?.text}"
          fs.write fd, table
        else
          console.log "Diff ¬found: #{jci}"
      catch e
        console.error e.message
