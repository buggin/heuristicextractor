request = require './wrapRequest'
$ = require 'cheerio'
urllib = require 'url'
{parseString} = require 'xml2js'
moment = require 'moment'
async = require 'async'
winston = require 'winston'
format = require './format'
wrapRequest = require './wrapRequest'
sanitizeHtml = require 'sanitize-html'
_ = require 'lodash'
MongoClient = require('mongodb').MongoClient
extractor = require('./heuristicExtractor')


log = new winston.Logger
  transports: [
    new winston.transports.Console { colorize:true, level: 'verbose'}
  ]

wait = (delay, callback) ->
    setTimeout callback, delay
sanitize = (html) ->
  return sanitizeHtml(html) if html

jaccard = (first, second) ->
  tokensFirst = first.split /\W+/
  tokensSecond = second.split /\W+/
  union = _.union(tokensFirst, tokensSecond)
  intersection = _.intersection(tokensFirst, tokensSecond)
  return _.uniq(intersection).length / _.uniq(union).length

exec = (cursor, limit, worker) ->
  queue = async.queue worker, limit
  queue.drain = () ->
    log.info "done"
  cursor.each (err, doc) ->
    queue.push doc if doc

MongoClient.connect "mongodb://ema:27017/extractor", (err, db) ->
  links = db.collection "classy"
  confront = db.collection "confront"

  cursor = links.find({clazz:{$ne:null}})

  exec cursor, 20, (site, ack) ->
    extractor.fetchData site.url, (err, objEx) ->
      log.error err.message if err
      return ack() if err 
      jci = jaccard objEx.text, site.text
      ratio = objEx.text.length / site.text.length
      # log.info "jci:#{jci}"
      confront.insert {url: site.url, jci: jci, ext: objEx.text, heu: site.text, clazz: site.clazz, ratio: ratio}, (err) ->
        log.error err.message if err
        return ack() if err
        # log.info "inserted " + site.url
        ack()