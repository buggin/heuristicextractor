MongoClient = require('mongodb').MongoClient
async = require 'async'
siblings = require '../extractors/siblingsExtractor'
orig = require '../extractors/schemaExtractor'
request = require 'request'
fs = require 'fs'
_ = require 'lodash'
urllib = require 'url'

jaccard = (first, second) ->
  return 1 if not first and not second
  return 0 if not first or not second
  tokensFirst = first.split /\W+/
  tokensSecond = second.split /\W+/
  union = _.union(tokensFirst, tokensSecond)
  intersection = _.intersection(tokensFirst, tokensSecond)
  return _.uniq(intersection).length / _.uniq(union).length

fs.open "html/siblings.html", "w", (err, fd) ->
  MongoClient.connect 'mongodb://ema:27017/extractor', (err, db) ->
    db.collection('binary').find().each (err, doc) ->
      if not doc
        console.log "Done!"
        return
      try
        v1 = siblings.fetchDataFromHTML doc.data.toString(), doc.url
        v2 = orig.fetchDataFromHTML doc.data.toString(), doc.url
        jci = jaccard v1.text, v2.text
        if jci < 0.8
          table = """
            <table border=1>
              <tr><td colspan=2>#{jci} : <a href="#{doc.url}">#{doc.url}</a></td></tr>
              <tr><td>#{v1.text}</td><td>#{v2.text}</td></tr>
            </table>
            <br><br>
          """
          console.log "Diff found: #{jci}"
          console.log "~~~~siblings~~~~#{v1.text} \n@@@@orig@@@@#{v2.text}"
          fs.write fd, table
        else
          console.log "Diff ¬found: #{jci}"
        
      catch e
        console.error e.message
