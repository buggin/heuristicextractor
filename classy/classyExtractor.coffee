extract = (url,clbck) ->
  wrapRequest.wrapRequest url, (err, body) -> 
    return clbck err if (err or not body)

    if body('entry-content').length
      res = count body, "articleBody"
      res.schemaType = "NewsArticle"
      return clbck null, res
