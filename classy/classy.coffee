sanitizeHtml = require 'sanitize-html'
$ = require 'cheerio'
fetcher = require './fetcher'

sanitize = (html) ->
  sanitizeHtml(html) if html

matchRegex = (e) ->
  e('div[id],article[id]').filter (x)->
    /post\-\d{4,}/.exec $(this).attr('id')

findDiv = (url,clbck) ->
  fetcher.getHTML url, (err, body) -> 
    return clbck err if (err or not body)
    clbck null, findMatchingClass body

findMatchingClass = (html) ->
  body = $.load(html)
  switch 
    # when body('.body-text').length
    #   {text: (sanitize body('.body-text')), clazz: "body-text"}#1
    # when body('.article-body').length
    #   {text: (sanitize body('.article-body')), clazz: "article-body"}#3
    when body('#entry').length is 1
      {text: (sanitize body('#entry')), clazz: "entry"}
    when body('.article-txt').length
      {text: (sanitize body('.article-txt')), clazz: "article-txt"}#5
    #when body('.field-item.even').length
    #  {text: (sanitize body('.field-item.even')), clazz: "field-item.even"}
    when body('.article-content').length
      {text: (sanitize body('.article-content')), clazz: "article-content"}#9
    when body('.entry-content').length
      {text: (sanitize body('.entry-content')), clazz: "entry-content"} #42

    when matchRegex(body).length
      {text: (sanitize matchRegex body), clazz: "post-number"}
    else
      {}
    #ora funziona che il primo che trova bom glielo assegna
    #dovrebbe provare per tutti quelli che trova 
      #e scegliere quello con il maggior testo


          #post_content
          #testoArticolo
          #article-main-content

exports.findDiv = findDiv
exports.findMatchingClass = findMatchingClass