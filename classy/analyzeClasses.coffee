$ = require 'cheerio'
urllib = require 'url'
{parseString} = require 'xml2js'
# moment = require 'moment'
async = require 'async'
winston = require 'winston'
# format = require './format'
fetcher = require './fetcher'
# sanitizeHtml = require 'sanitize-html'
_ = require 'lodash'
MongoClient = require('mongodb').MongoClient
classy = require './classy'
fs = require 'fs'
diff = require './htmldiff'
extractor = require './heuristicExtractor'
schema = require './schema'

log = new winston.Logger
  transports: [
    new winston.transports.Console { colorize:true, level: 'verbose'}
  ]

wait = (delay, callback) ->
  setTimeout callback, delay

exec = (cursor, limit, worker) ->
  queue = async.queue worker, limit
  queue.drain = () ->
    log.info "done"
  cursor.forEach (doc) ->
    queue.push doc if doc

firstUrl = (site, callback) -> 
  fetcher.getHTML site, (err, body) ->
    return callback err if err
    xml = body
    parseString xml, (err, result) ->
      return callback err if err 
      return callback new Error("no data from xml"), null if not result or not xml
      url = result.rss?.channel?[0]?.item?[0]?.link?[0] or result.feed?.entry?[0]?.link?[0]['$'].href
      return callback new Error("no links"), null if not url
      return callback null, urllib.resolve(site, url)

jaccard = (first, second) ->
  tokensFirst = first.split /\W+/
  tokensSecond = second.split /\W+/
  union = _.union(tokensFirst, tokensSecond)
  intersection = _.intersection(tokensFirst, tokensSecond)
  return _.uniq(intersection).length / _.uniq(union).length

class Diff 
  constructor: (clazz) ->
    @fd = fs.openSync("html/#{clazz}.html", "w")
    fs.write @fd, """
    <style>
      .red, .red a {color:red !important;}
      .black, .black a {color:black;}
      ins {color: green;}
      del {color: red; text-decoration: none;}
    </style>
    """

  push: (first, second, url) ->
    j = jaccard(first, second)
    diffedText = diff first, second
    fs.writeSync @fd, """
      <table border=1>
        <tr><td colspan=2> -cut- #{j} - <a target="_blank" href="#{url}">#{url}</a></td></tr>
        <tr><td>#{diffedText}</td><td>#{first}</td></tr>
      </table>
      <br><br>
    """

classes = ["body-text", "article-body", "article-txt", "field-item.even", "article-content", "entry-content","post-number", "entry"]
diffs = {}
for clazz in classes
  diffs[clazz] = new Diff(clazz)

amqp = require('amqplib').connect 'amqp://ebezzi:ebezzi@ema'

# MongoClient.connect "mongodb://ema:27017/extractor", (err, db) ->
#   links = db.collection "diffs"

amqp.then (conn) ->

  conn.createChannel().then (linkChannel) ->
    linkChannel.assertQueue 'extractor'
    linkChannel.prefetch 25

    linkChannel.consume 'extractor', (msg) ->
      url = msg.content.toString()
      fetcher.getHTML url, (err, html) -> 
        if err
          linkChannel.ack msg
          return log.error err.message 
        body = $.load(html)
        if schema.boolImpl body
          linkChannel.ack msg
          return log.verbose "Schema"
        original = extractor.fetchDataFromHTML html, url
        {clazz: matchedClass, text: matchedText} = classy.findMatchingClass html

        if matchedClass
          log.info "Matched class %s for %s", matchedClass, url
          diffs[matchedClass].push(original.text, matchedText, url)
        else 
          log.info "No match"

        linkChannel.ack msg