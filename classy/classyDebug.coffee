moment = require 'moment'
_ = require 'lodash'
winston = require 'winston'
classy = require('./classy')
fetcher = require './fetcher'
log = new winston.Logger
  transports: [
    new winston.transports.Console { colorize:true, level: 'verbose'}
  ]

url = process.argv[2]

classy.findDiv url, (err, obj) ->
        log.error err.message if err
        log.info obj.clazz if obj
        # log.info obj.text