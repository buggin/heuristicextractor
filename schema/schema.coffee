fetcher = require '../fetcher'
$ = require 'cheerio'
_ = require 'lodash'
sanitizeHtml = require 'sanitize-html'

sanitize = (html) ->
  return sanitizeHtml(html) if html

count = (body, section) ->
  candidate = body("[itemprop~='#{section}']")
  stext = sanitize candidate.html() 
  if stext
    return {schema: "implBene", text: stext, div: candidate}
  else
    stext = body("[itemprop~='#{section}']").attr('content') #non sono sicuro del perche abbia hardcoded articleBody
    if stext
      return {schema: "implMeta", text: stext}
    else
      return {schema: "implMale", text: "" }

extract = (url,clbck) ->
  fetcher.getDOM url, (err, body) -> 
    return clbck err if (err or not body)
    checkImpl body, clbck
    
checkImpl = (body, clbck) ->
  if body('[itemtype="http://schema.org/NewsArticle"]').length
    res = count body, "articleBody"
    res.schemaType = "NewsArticle"
    return clbck null, res

  if body('[itemtype="http://schema.org/Article"]').length
    res = count body, "articleBody"
    res.schemaType = "Article"
    return clbck null, res

  if body('[itemtype="http://schema.org/BlogPosting"]').length
    res = count body, "articleBody"
    res.schemaType = "BlogPosting"
    return clbck null, res

  if body('[itemtype="http://schema.org/WebPage"]').length
    res = count body, "text"
    res.schemaType = "WebPage"
    return clbck null, res

  if body('[itemtype="http://schema.org/Review"]').length
    res = count body, "reviewBody"
    res.schemaType = "Review"
    return clbck null, res

  clbck null, {schema: "nonImpl"}

getText = (body) ->
  checkImpl body, (err, obj) ->
    return "Error: #{err}" if err
    obj.text
    

boolImpl = (body) ->
  checkImpl body, (err, obj) ->
    return false if (err or obj.schema is "nonImpl" or obj.schema is "implMale")
    true

exports.extract = extract
exports.checkImpl = checkImpl
exports.boolImpl = boolImpl
exports.getText = getText
